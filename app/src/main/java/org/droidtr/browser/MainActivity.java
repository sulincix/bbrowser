package org.droidtr.browser;

import android.app.*;
import android.content.*;
import android.database.*;
import android.graphics.*;
import android.net.*;
import android.os.*;
import android.provider.*;
import android.text.*;
import android.view.*;
import android.view.View.*;
import android.view.inputmethod.*;
import android.webkit.*;
import android.webkit.WebView.*;
import android.widget.*;
import android.widget.AdapterView.*;
import java.io.*;
import java.text.*;
import java.util.*;
import org.droidtr.browser.clients.*;

import android.view.View.OnLongClickListener;
import android.webkit.WebChromeClient.*;

public class MainActivity extends Activity {
	
	String[] onion = {".onion",".onion.link",".onion.cab",
						".tor2web.org",".onion.sh",".tor2web.fi",
						".onion.direct",".onion.ru",".onion.city",
						".onion.to"};
	Random r= new Random();
	String x0 = new String();
	WebView browser;
	WebSettings bsettings;
	EditText url;
	ProgressBar prg;
	DownloadManager dm;
	boolean isfullcsreen=false;
	boolean start = true;
	boolean focus = false;
	boolean longc = false;
	boolean pause = false;
	boolean scrll = false;
	boolean locpr = false;
	long enq;
	String fname;
	Uri uri;
	SharedPreferences sp;
	SharedPreferences.Editor se;
	ImageButton del;
	ImageButton rbt;
	ImageButton ourl;
	ImageButton newtab;
	ImageButton set;
	ImageView favicon;
	
	LinearLayout pbg;
	RelativeLayout rell;
	int osel;
	int otmp;
	int x;
	int y;
	String ca;
	View v;
	BroadcastReceiver receiver;

	private String lasturl="";

	ImageButton rfwd;
	ImageButton rbck;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		getActionBar().setCustomView(R.layout.actionbar);
		getActionBar().setDisplayShowCustomEnabled(true);
		v = getActionBar().getCustomView();
		getActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.abar));
		getActionBar().setIcon(android.R.color.transparent);
		sp = getSharedPreferences(getPackageName()+".settings",Context.MODE_PRIVATE);
		se = sp.edit();
		ca = sp.getString("customAgent","");
		del = (ImageButton) v.findViewById(R.id.del);
		rbt = (ImageButton) v.findViewById(R.id.refstop);
		rbck = (ImageButton) v.findViewById(R.id.back);
		rfwd = (ImageButton) v.findViewById(R.id.forward); 
		ourl = (ImageButton) v.findViewById(R.id.open);
		newtab = (ImageButton) v.findViewById(R.id.newtab);
		set = (ImageButton) v.findViewById(R.id.sett);
		otmp = sp.getInt("onionSelection",1);
		pbg = (LinearLayout) findViewById(R.id.pbg);
		rell = (RelativeLayout) findViewById(R.id.rell);
		browser = (WebView) findViewById(R.id.browser);
		url = (EditText) v.findViewById(R.id.url);
		prg = (ProgressBar) findViewById(R.id.prg);
		if(Build.VERSION.SDK_INT <=21){
			newtab.setVisibility(View.GONE);
		}
		ourl.setVisibility(View.GONE);
		del.setVisibility(View.GONE);
		url.setPadding(20,0,20,0);
		
		x = browser.getScrollX();
		y = browser.getScrollY();
		
		newtab.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					Intent intent = new Intent(MainActivity.this,MainActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
					intent.addCategory(Intent.CATEGORY_LAUNCHER);
					startActivity(intent);

				}
			});
		receiver = new BroadcastReceiver(){
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
					DownloadManager.Query query = new DownloadManager.Query();
					query.setFilterById(enq);
					Cursor c = dm.query(query);
					if (c.moveToFirst()) {
						int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
						if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex))
							bildirim(sp.getString("latestFile",""));
						unregisterReceiver(receiver);
						se.putString("latestFile","");
						se.commit();
					}
				}
			}
		};
		browser.setWebViewClient(new MyBrowser());
		browser.setWebChromeClient(new MyChromeClient());
		browser.addJavascriptInterface(new org.droidtr.browser.clients.BlinkJSClient(),"android");
		browser.setDownloadListener(new DownloadListener() {
				public void onDownloadStart(String url, String userAgent,
											String contentDisposition, String mimetype,
											long contentLength){
					registerReceiver(receiver, new IntentFilter(
										 DownloadManager.ACTION_DOWNLOAD_COMPLETE));
					download(url);
				}
			});
		bsettings = browser.getSettings();
		bsettings.setAppCacheEnabled(true);
		bsettings.setSupportZoom(true);
		bsettings.setBuiltInZoomControls(false);
		bsettings.setPluginState(WebSettings.PluginState.ON);
		bsettings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
		bsettings.setJavaScriptEnabled(true);
		bsettings.setGeolocationEnabled(true);
		bsettings.setLightTouchEnabled(true);
		
		if(Build.VERSION.SDK_INT >= 19){
			browser.setLayerType(View.LAYER_TYPE_HARDWARE, null);
			getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
				WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
		} else {
			browser.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			if(Build.VERSION.SDK_INT < 18)
				bsettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
		}


		url.setOnKeyListener(new View.OnKeyListener() {
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
						(keyCode == KeyEvent.KEYCODE_ENTER)) {
						loadPage(false);
						unfocus();
						hideKbd();
						return true;
					}
					return false;
				}
			});

		ourl.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					loadPage(false);
					unfocus();
					hideKbd();
				}
			});

		ourl.setOnLongClickListener(new View.OnLongClickListener(){
				@Override
				public boolean onLongClick(View v){
					Toast.makeText(getBaseContext(),getResources().getString(R.string.ugit),Toast.LENGTH_SHORT).show();
					return false;
				}
			});

		newtab.setOnLongClickListener(new View.OnLongClickListener(){
				@Override
				public boolean onLongClick(View v){
					Toast.makeText(getBaseContext(),getResources().getString(R.string.sind),Toast.LENGTH_SHORT).show();
					return false;
				}
			});

		rbt.setOnLongClickListener(new View.OnLongClickListener(){
				@Override
				public boolean onLongClick(View v){
					Toast.makeText(getBaseContext(),getResources().getString(R.string.synl),Toast.LENGTH_SHORT).show();
					return false;
				}
			});
		rbck.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1)
				{
					browser.goBack();
				}
			});
		rfwd.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1)
				{
					browser.goForward();
				}
			});
		
		browser.setOnTouchListener(new View.OnTouchListener(){
				@Override
				public boolean onTouch(View v, MotionEvent m){
					if(url.isFocused()){
						unfocus();
						hideKbd();
					}
					return false;
				}
			});

		browser.setOnLongClickListener(new OnLongClickListener(){
				@Override
				public boolean onLongClick(View v){
					linkAlgila();
					return false;
				}
			});

		del.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					url.setText("");
				}
			});

		url.setOnLongClickListener(new View.OnLongClickListener(){
				@Override
				public boolean onLongClick(View v){
					longc = true;
					return false;
				}
			});

		url.setOnFocusChangeListener(new View.OnFocusChangeListener(){
				@Override
				public void onFocusChange(View v, boolean f){
					try{
						focus = f;
						boolean b = url.getText().toString().length()>0;
						runBackground();
						detectETFocus(b && f);
						if(f){
							if(!longc){
								url.setSelection(0,url.getText().toString().length());
								((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).showSoftInput(v,InputMethodManager.SHOW_IMPLICIT);
							} else longc = false;
						} else {
							unfocus();
							hideKbd();
						}
					} catch(Exception e){}
				}
			});

		url.addTextChangedListener(new TextWatcher(){
				public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4){}
				public void afterTextChanged(Editable p1){}
				public void onTextChanged(CharSequence p1, int p2, int p3, int p4){ detectETFocus(p1.length() > 0 && url.isFocused()); }
			});

		
		if(Build.VERSION.SDK_INT >= 23){
			browser.setOnScrollChangeListener(new View.OnScrollChangeListener(){
					@Override
					public void onScrollChange(View p1, int p2, int p3, int p4, int p5){
						fullScreen();
						if(url.isFocused()){
							unfocus();
							hideKbd();
						}
					}
				});
		} else {
			scrollListener();
		}

		browser.setOnFocusChangeListener(new View.OnFocusChangeListener(){
				@Override
				public void onFocusChange(View v,boolean f){
					fullScreen();
					if(url.isFocused()){
						unfocus();
						hideKbd();
					}
				}
			});

		set.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					unfocus();
					hideKbd();
					if(!sp.getBoolean("backPlay",false)){
						browser.onPause();
						browser.pauseTimers();
					}
					startActivity(new Intent(MainActivity.this,Settings.class));
				}
			});

		set.setOnLongClickListener(new View.OnLongClickListener(){
				@Override
				public boolean onLongClick(View v){
					Toast.makeText(getBaseContext(),getResources().getString(R.string.ayar),Toast.LENGTH_SHORT).show();
					return false;
				}
			});
			if(lasturl!=""){
				browser.loadUrl(lasturl);
			}
			else{
			browser.loadUrl("https://start.duckduckgo.com");
			}
			fullScreen();
    }
	
	void actionBar(){
		new Handler().postDelayed(new Runnable(){
			@Override
			public void run(){
				try{
					if(url.isFocused()){
						if(getActionBar().isShowing() && sp.getBoolean("fullScreen",false))
							rell.setPadding(0,getActionBar().getHeight()+60,0,0);
						else rell.setPaddingRelative(0,0,0,0);
					} else rell.setPadding(0,0,0,0);
					if(!start && !url.isFocused() && (url.getText().toString() != browser.getUrl()))
						url.setText(sReplacer(browser.getUrl()));
					actionBar();
				} catch(Exception e){}
			}
		},sure(getActionBar().isShowing()));
	}
	
	void loadPage(boolean sPage){
		x0 = urlParser(url.getText().toString());
		if(x0.length() > 0){
			if(sp.getInt("userAgent",1) == 0)
				bsettings.setUserAgentString("Mozilla/5.0 (iPhone; U; 12.1 like Mac OS X; en) Gecko/20100101 Firefox/60.0");
			if(sp.getInt("userAgent",1) == 1)
				bsettings.setUserAgentString("Mozilla/5.0 (Android ;"+Build.VERSION.RELEASE+"; rv:60.0) Gecko/20100101 Firefox/60.0");
			if(sp.getInt("userAgent",1) == 2)
				bsettings.setUserAgentString("Mozilla/5.0 (X11; Linux aarch64; rv:60.0) Gecko/20100101 Firefox/60.0");
			if(sp.getInt("userAgent",1) == 3)
				bsettings.setUserAgentString(new RandomUA().getMobileAgent());
			if(sp.getInt("userAgent",1) == 4)
				bsettings.setUserAgentString(new RandomUA().getDesktopAgent());
			if(sp.getInt("userAgent",1) == 5)
				bsettings.setUserAgentString(new RandomUA().getBotAgent());
			if(sp.getInt("userAgent",1) == 6)
				bsettings.setUserAgentString(new RandomUA().getFunAgent());
			if(sp.getInt("userAgent",1) == 7)
				bsettings.setUserAgentString(ca);
			if(sPage){
				browser.loadUrl("about:blank");
				start = true;
			} else {
				browser.loadUrl(urlParser(x0));
				start = false;
			}
		}
	}
	
	int sure(boolean b){
		if(b) return 2000;
		return 100;
	}
	
	void runBackground(){
		new Handler().postDelayed(new Runnable(){
			@Override
			public void run(){
				try{
					osel = sp.getInt("onionSelection",1);
					if(start || browser.getUrl().contains("about:blank")) {
						rbt.setEnabled(false);
					} else {
						rbt.setEnabled(true);
					}
					
					if(otmp != osel){
						otmp = osel;
						if(!start && browser.getUrl().contains(".onion"))
							loadPage(false);
					}
					
					if(browser.canGoBack()){
						rbck.setVisibility(View.VISIBLE);
					}else{
						rbck.setVisibility(View.GONE);
					}
					if(browser.canGoForward()){
						rfwd.setVisibility(View.VISIBLE);
					}else{
						rfwd.setVisibility(View.GONE);
					}
					if(!url.isFocused()){
						if(browser.getUrl() != null){
							if(!start) url.setText(browser.getUrl());
							if(browser.getUrl().contains(".onion"))
								url.setText(sReplacer(browser.getUrl()));
							else {
								if(browser.getUrl().contains("about:blank")) {
									url.setText("");
								} else url.setText(browser.getUrl());
							}
						}
					}
				} catch(Exception e){}
			}
		},50);
	}
	
	private class MyBrowser extends BlinkWebClient{
		
		@Override
		public void onPageFinished(WebView view, String url){
			pageLoadFinished();
			if(Build.VERSION.SDK_INT > 21){
			int color = Color.rgb(r.nextInt(255),r.nextInt(255),r.nextInt(255));
				setTaskDescription(new ActivityManager.TaskDescription(view.getTitle(),view.getFavicon(),color));
			}
				super.onPageFinished(view, url);
		}
	}
	
	ValueCallback<Uri[]> fpc;
	private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
			Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(
			imageFileName,  /* prefix */
			".jpg",         /* suffix */
			storageDir      /* directory */
        );
        return imageFile;
	}
	private class MyChromeClient extends BlinkWebChromeClient
	{

		FrameLayout.LayoutParams LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        RelativeLayout mContentView;
		FrameLayout mCustomViewContainer;
		@Override
		public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback)
		{
			isfullcsreen=true;
			fullScreen();
			lasturl=browser.getUrl();
			setContentView(view);
		}

		@Override
		public void onHideCustomView()
		{
			isfullcsreen=false;
			onCreate(new Bundle());
			super.onHideCustomView();
		}
		
		
		@Override
		public void onReceivedTitle(WebView view, String title)
		{
			if(Build.VERSION.SDK_INT > 21){
			int color = Color.rgb(r.nextInt(255),r.nextInt(255),r.nextInt(255));
			setTaskDescription(new ActivityManager.TaskDescription(view.getTitle(),view.getFavicon(),color));
			}
			super.onReceivedTitle(view, title);
		}
		
		
	
		// For Android 5.0
        public boolean onShowFileChooser(WebView view, ValueCallback<Uri[]> filePath, WebChromeClient.FileChooserParams fileChooserParams) {
            // Double check that we don't have any existing callbacks
            if (mFilePathCallback != null) {
                mFilePathCallback.onReceiveValue(null);
            }
            mFilePathCallback = filePath;
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                    takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
                } catch (Exception ex) {
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
											   Uri.fromFile(photoFile));
                } else {
                    takePictureIntent = null;
                }
            }
            Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
            contentSelectionIntent.setType("image/*");
            Intent[] intentArray;
            if (takePictureIntent != null) {
                intentArray = new Intent[]{takePictureIntent};
            } else {
                intentArray = new Intent[0];
            }
            Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
            chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
            chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
            startActivityForResult(chooserIntent, INPUT_FILE_REQUEST_CODE);
            return true;
        }
        // openFileChooser for Android 3.0+
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
            mUploadMessage = uploadMsg;
            // Create AndroidExampleFolder at sdcard
            // Create AndroidExampleFolder at sdcard
            File imageStorageDir = new File(
				Environment.getExternalStoragePublicDirectory(
					Environment.DIRECTORY_PICTURES)
				, "AndroidExampleFolder");
            if (!imageStorageDir.exists()) {
                // Create AndroidExampleFolder at sdcard
                imageStorageDir.mkdirs();
            }
            // Create camera captured image file path and name
            File file = new File(
				imageStorageDir + File.separator + "IMG_"
				+ String.valueOf(System.currentTimeMillis())
				+ ".jpg");
            mCapturedImageURI = Uri.fromFile(file);
            // Camera capture image intent
            final Intent captureIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");
            // Create file chooser intent
            Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
            // Set camera intent to file chooser
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS
								   , new Parcelable[] { captureIntent });
            // On select image call onActivityResult method of activity
            startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
        }
        // openFileChooser for Android < 3.0
        public void openFileChooser(ValueCallback<Uri> uploadMsg) {
            openFileChooser(uploadMsg, "");
        }
        //openFileChooser for other Android versions
        public void openFileChooser(ValueCallback<Uri> uploadMsg,
                                    String acceptType,
                                    String capture) {
            openFileChooser(uploadMsg, acceptType);
        }
	
		
		
		
		@Override
		public void onProgressChanged(WebView view, int newProgress){
			pbg.setVisibility(View.VISIBLE);
			prg.setProgress(newProgress);
			if(newProgress > 98)
				pageLoadFinished();
			else pageLoading();
			runBackground();
			super.onProgressChanged(view, newProgress);
		}
		
		AlertDialog d;
		
		public void onGeolocationPermissionsShowPrompt(final String origin, final GeolocationPermissions.Callback callback) {
			LinearLayout ll = new LinearLayout(MainActivity.this);
			ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
			ll.setPadding(64,8,64,0);
			CheckBox cb = new CheckBox(MainActivity.this);
			cb.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
			cb.setText(getResources().getString(R.string.sech));
			ll.addView(cb);
			cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
					@Override
					public void onCheckedChanged(CompoundButton p1, boolean p2){
						se.putBoolean("locPrompt",p2);
						locpr = p2;
					}
			});
			
			String[] temp = origin.replace("http://","").replace("https://","").split("/");
			
			d = new AlertDialog.Builder(MainActivity.this)
				.setTitle(getResources().getString(R.string.koni))
				.setMessage(String.format(getResources().getString(R.string.kono),temp[0]))
				.setPositiveButton(getResources().getString(R.string.evet), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) { 
						callback.invoke(origin, true, locpr);
						se.commit();
						d.dismiss();
					}
				})

				.setNegativeButton(getResources().getString(R.string.hayr), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) { 
						callback.invoke(origin, false, locpr);
						se.commit();
						d.dismiss();
					}
				})
				.setView(ll).create();
			d.show();
			
		}
		
	}
	
	void pageLoadFinished(){
		pbg.setVisibility(View.GONE);
		prg.setProgress(0);
		rbt.setImageDrawable(getResources().getDrawable(R.drawable.reload));
		rbt.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				loadPage(false);
			}
		});
	}
	void pageLoading(){
		runBackground();
		rbt.setImageDrawable(getResources().getDrawable(R.drawable.stop));
		rbt.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				browser.stopLoading();
			}
		});
	}
	
	void bildirim(String s){
		String x = s.substring(s.lastIndexOf("."));
		x = x.replace(".","");
		Intent toLaunch = new Intent();
		toLaunch.setAction(android.content.Intent.ACTION_VIEW);
		toLaunch.setDataAndType(Uri.fromFile(new File("/sdcard/Android/data/blink.tor/files/Download/"+s)), MimeTypeMap.getSingleton().getMimeTypeFromExtension(x));
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, toLaunch, 0);
		NotificationManager nMN = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Notification n  = new Notification.Builder(this)
			.setContentIntent(pIntent)
			.setContentTitle(s)
			.setContentText("Download completed")
			.setSmallIcon(android.R.drawable.stat_sys_download_done)
			.setPriority(Notification.PRIORITY_DEFAULT)
			.build();
		nMN.notify(new Random().nextInt(9),n);
	}
	
	String removeLast(String s){
		return s.substring(0,s.length()-1);
	}
	
	String urlParser(String x0){
		x0 = sReplacer(x0);
		if(x0.trim().length() > 0 && !x0.contains("about:")){
			if(!x0.contains(onion[osel])){
				if(!x0.contains(".")){
					if(!sp.getBoolean("nBrowser",true))
						x0 = "https://xmh57jrzrnw6insl"+onion[osel]+"/4a1f6b371c/search.cgi?q="+x0+"&cmd=Search%21";
					else
						x0 = "http://duckduckgo.com/?q="+x0;
				}
				else {
					if(x0.endsWith(".")){
						if(!sp.getBoolean("nBrowser",true))
							x0 = "https://xmh57jrzrnw6insl"+onion[osel]+"/4a1f6b371c/search.cgi?q="+removeLast(x0)+"&cmd=Search%21";
						else
							x0 = "http://duckduckgo.com/?q="+removeLast(x0);
					}
					if(x0.contains(".onion")) x0 = x0.replace(".onion",onion[osel]);
					if(!x0.contains("://")) x0 = "http://"+x0;
				}
			}
			return x0;
		} else return "about:blank";
		
	}
	
	void download(String url){
		uri = Uri.parse(url);
		Intent intent=new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(uri,"*/*");
		startActivity(intent);
	}
	
	void linkAlgila(){
		HitTestResult result = browser.getHitTestResult();
		if(result.getType() == HitTestResult.IMAGE_TYPE ||
			result.getType() == HitTestResult.SRC_IMAGE_ANCHOR_TYPE) menuAc(result.getExtra(),0);
		else if(result.getType() == HitTestResult.ANCHOR_TYPE ||
				   result.getType() == HitTestResult.SRC_ANCHOR_TYPE) menuAc(result.getExtra(),1);
	}
	
	String sReplacer(String s){
		for(int i = 1;i!=onion.length;i++)
			if(s.contains(onion[0])) s = s.replaceFirst(onion[i],onion[0]);
		return s;
	}
	
	AlertDialog ad;
	
	void menuAc(final String baslikMetni, final int menuTipi){
		ArrayAdapter<String> dStr = new ArrayAdapter<String>
		(this, android.R.layout.simple_list_item_1, android.R.id.text1);
		ListView lv = new ListView(this);
		lv.setPadding(8,8,8,8);
		lv.setLayoutParams(new ListView.LayoutParams(
								ListView.LayoutParams.MATCH_PARENT, 
								ListView.LayoutParams.WRAP_CONTENT));
		if(menuTipi == 0) {
			dStr.add(getResources().getString(R.string.rac1));
			dStr.add(getResources().getString(R.string.rac2));
			dStr.add(getResources().getString(R.string.rac3));
			dStr.add(getResources().getString(R.string.rac4));
		} else {
			dStr.add(getResources().getString(R.string.sac1));
			dStr.add(getResources().getString(R.string.sac2));
			dStr.add(getResources().getString(R.string.sac3));
			dStr.add(getResources().getString(R.string.sac4));
		}
		lv.setAdapter(dStr);
		lv.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> av, View v, int p, long id){
				ad.dismiss();
				if(p == 0){ url.setText(baslikMetni); loadPage(false); } 
				if(p == 1) startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(baslikMetni)));
				if(p == 2) metniKopyala(baslikMetni);
				if(p == 3) {
					if(menuTipi==1){
					SharedPreferences.Editor se = (getSharedPreferences(getPackageName()+".settings",Context.MODE_PRIVATE)).edit();
					se.putString("url",baslikMetni);
					se.commit();
					Intent intent = new Intent(MainActivity.this,MainActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
					intent.addCategory(Intent.CATEGORY_LAUNCHER);
					startActivity(intent);
					}
				}
			}
		});
		ad = new AlertDialog.Builder(this).setTitle(baslikMetni).setView(lv).create();
		ad.show();
	}
	
	String temp;
	
	void metniKopyala(String s){
		if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
			android.text.ClipboardManager pano = (android.text.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
			if(s.length() > 0) pano.setText(s);
			else temp = pano.getText().toString();
		} else {
			android.content.ClipboardManager pano = (android.content.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
			android.content.ClipData clip = android.content.ClipData.newPlainText(getResources().getString(R.string.btor), s);
			if(s.length() > 0) pano.setPrimaryClip(clip);
			else temp = pano.getText().toString();
		}
		if(s.length() > 0)
			Toast.makeText(getBaseContext(),getResources().getString(R.string.mkpy),Toast.LENGTH_LONG).show();
	}
	
	void detectETFocus(boolean b){
		try{
			if(b){
				ourl.setVisibility(View.VISIBLE);
				del.setVisibility(View.VISIBLE);
				rbt.setVisibility(View.GONE);
				url.setPaddingRelative(20,0,20+del.getWidth(),0);
				if(!focus){
					focus = true;
					if(url.getText().toString().length() > 1)
						url.setSelection(0,url.getText().toString().length());	
				}
			} else {
				ourl.setVisibility(View.GONE);
				del.setVisibility(View.GONE);
				rbt.setVisibility(View.VISIBLE);
				url.setPaddingRelative(20,0,20,0);
				url.setSelection(0); focus = false;
			}
		} catch(Exception e){}
	}
	
	void disableCache(boolean disable){
		try{
			if(disable){
				bsettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
				bsettings.setSaveFormData(false);
			} else {
				bsettings.setCacheMode(WebSettings.LOAD_DEFAULT);
				bsettings.setSaveFormData(true);
			} 
		} catch(Exception e){}
	}
	
	void fullScreen(){
		try{
			if(isfullcsreen || sp.getBoolean("fullScreen",false)){
						getWindow().getDecorView().setSystemUiVisibility(
						View.SYSTEM_UI_FLAG_LAYOUT_STABLE
						| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
						| View.SYSTEM_UI_FLAG_FULLSCREEN
						| View.SYSTEM_UI_FLAG_IMMERSIVE);
			} else {
				getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
			}
		} catch(Exception e){}
	}
	
	void unfocus(){
		try{
			if(focus){
				url.setFocusable(false);
				url.setFocusable(true);
				url.setFocusableInTouchMode(false);
				url.setFocusableInTouchMode(true);
			}
		} catch(Exception e){}
	}
	
	void hideKbd(){
		
		((InputMethodManager) getSystemService(
			Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
			url.getWindowToken(), 0);
			
	}
	
	void getURL(){
		try{
			String gurl = sReplacer(sp.getString("url",""));
			if(gurl.length() > 0){
				start = false;
				url.setText(gurl);
				browser.loadUrl("about:blank");
				loadPage(false);
				se.remove("url");
				se.commit();
			}
		} catch(Exception e){}
	}
	
	@Override
	public void onBackPressed(){
		try{
			if(url.isFocused()) unfocus();
			else {
				if(browser.canGoBack()) browser.goBack();
				else {
					if(start) super.onBackPressed();
					else loadPage(true);
				}
			}
		} catch(Exception e){}
	}

	@Override
	protected void onPause(){
		super.onPause();
		try{
			unfocus();
			hideKbd();
			pause = true;	
			if(!sp.getBoolean("backPlay",false)){
				browser.onPause();
				browser.pauseTimers();
			}
		} catch(Exception e){}
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		try{
			browser.onResume();
			browser.resumeTimers();
			actionBar();
			fullScreen();
			getURL();
			runBackground();
			disableCache(sp.getBoolean("disableCache",false));
			pause = false;
			if((url.getText().toString().length() > 0) && 
			(start || (browser.getUrl() == null) || 
			(browser.getUrl() == "about:blank")))
				loadPage(false);
		} catch(Exception e){}
	}
	
	private long lastMoveEventTime = -1;
    private int eventTimeInterval = 40;

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        long eventTime = ev.getEventTime();
        int action = ev.getAction();

        switch (action){
            case MotionEvent.ACTION_MOVE: {
                if ((eventTime - lastMoveEventTime) > eventTimeInterval){
                    lastMoveEventTime = eventTime;
                    return super.onTouchEvent(ev);
                }
                break;
            }
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_UP: {
                return super.onTouchEvent(ev);
            }
        }
        return true;
    }
	private static final int INPUT_FILE_REQUEST_CODE = 1;
    private static final int FILECHOOSER_RESULTCODE = 1;
	private ValueCallback<Uri> mUploadMessage;
    private Uri mCapturedImageURI = null;
    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode != INPUT_FILE_REQUEST_CODE || mFilePathCallback == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
            Uri[] results = null;
            // Check that the response is a good one
            if (resultCode == Activity.RESULT_OK) {
                if (data == null) {
                    // If there is not data, then we may have taken a photo
                    if (mCameraPhotoPath != null) {
                        results = new Uri[]{Uri.parse(mCameraPhotoPath)};
                    }
                } else {
                    String dataString = data.getDataString();
                    if (dataString != null) {
                        results = new Uri[]{Uri.parse(dataString)};
                    }
                }
            }
            mFilePathCallback.onReceiveValue(results);
            mFilePathCallback = null;
        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (requestCode != FILECHOOSER_RESULTCODE || mUploadMessage == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
            if (requestCode == FILECHOOSER_RESULTCODE) {
                if (null == this.mUploadMessage) {
                    return;
                }
                Uri result = null;
                try {
                    if (resultCode != RESULT_OK) {
                        result = null;
                    } else {
                        // retrieve from the private variable if the intent is null
                        result = data == null ? mCapturedImageURI : data.getData();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "activity :" + e,
								   Toast.LENGTH_LONG).show();
                }
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
        }
        return;
    }
	
	
	void scrollListener(){
		new Handler().postDelayed(new Runnable(){
			public void run(){
				if(!start){
					if((browser.getScrollX()!=x) || (browser.getScrollY()!=y)){
						x = browser.getScrollX();
						y = browser.getScrollY();
						if(url.isFocused()){
							unfocus();
							hideKbd();
						}
					}
				}
				scrollListener();
			}
		},250);
	}
}
