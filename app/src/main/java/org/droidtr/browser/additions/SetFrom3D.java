package org.droidtr.browser.additions;

import android.app.*;
import android.content.*;
import android.os.*;
import org.droidtr.browser.*;

public class SetFrom3D extends Activity{
	public void onCreate(Bundle b){
		super.onCreate(b);
		overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
		new Handler().postDelayed(new Runnable(){
			public void run(){
				finish();
				startActivity(new Intent(SetFrom3D.this,Settings.class).setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
				overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
			}
		},1500);
	}
}
